# network programs

Examples of python programs that uses networking and libs (socket, urllib, beautiful soup)

# Development usage

You need Python above version 3.10 and pip installed

1. Clone the repository
2. Create a virtual environment https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment
3. Activate the virtual environment https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#activating-a-virtual-environment
4. Install requirements `pip install -r requirements.txt`
5. Run one of the applications
