"""
Exercise 1: Change the socket program socket1.py to prompt the user
for the URL so it can read any web page. You can use split('/') to
break the URL into its component parts so you can extract the host
name for the socket connect call. Add error checking using try and
except to handle the condition where the user enters an improperly
formatted or non-existent URL.
"""

import socket

mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# input url, use default if nothing entered
url = input('Enter a url: ')
if url == "":
    # url = 'http://data.pr4e.org/romeo.txt'
    url = 'https://www.w3.org/TR/PNG/iso_8859-1.txt'

# extract domain from url
domain = url.split("//")[-1].split("/")[0].split('?')[0]

# connect a socket on port 80 to the domain
mysock.connect((domain, 80))

# build and encode the http GET command
cmd = 'GET ' + url + ' HTTP/1.0\r\n\r\n'
cmd_encoded = cmd.encode()

# send the byte encoded GET command
mysock.send(cmd_encoded)

# receive 512 chars at a time, decode and print until empty line received
while True:
    data = mysock.recv(512)
    if len(data) < 1:
        break
    print(data.decode(),end='')

# close socket
mysock.close()